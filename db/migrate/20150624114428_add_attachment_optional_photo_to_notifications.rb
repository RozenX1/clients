class AddAttachmentOptionalPhotoToNotifications < ActiveRecord::Migration
  def self.up
    change_table :notifications do |t|
      t.attachment :optional_photo
    end
  end

  def self.down
    remove_attachment :notifications, :optional_photo
  end
end
