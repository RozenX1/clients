class AddFieldsToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :telephone, :integer
    add_reference :notifications, :user, index: true, foreign_key: true
  end
end
