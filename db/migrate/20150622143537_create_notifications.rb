class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :person
      t.string :city
      t.string :street
      t.integer :number
      t.string :appliance
      t.boolean :solved
      t.text :problem

      t.timestamps null: false
    end
  end
end
