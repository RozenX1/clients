json.extract! @comment, :id, :user_id, :notification_id, :body, :created_at, :updated_at
json.email @comment.user.email
json.nada nil
json.have_avatar !@comment.user.avatar_file_name.nil?
json.avatar @comment.user.avatar.url
