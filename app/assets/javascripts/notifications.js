$(document).on("ready",function(){
	$(".toggle-solved-btn").on("click", function(){
		if($(this).hasClass("btn-warning")){
			$(this).addClass("btn-success").removeClass("btn-warning")
			$(this).html('<span class="glyphicon glyphicon-ok-sign"></span> Marcar como resuelto')
			$(".label-toggle-solved").html('<span class="label label-danger">Sin resolver</span>')
		}else{
			$(this).addClass("btn-warning").removeClass("btn-success")
			$(this).html('<span class="glyphicon glyphicon-remove-sign"></span> Marcar como reabierto')
			$(".label-toggle-solved").html('<span class="label label-success">Resuelto</span>')
		};
	});

	$("#btn-modal-register-now").trigger("click");
});
