class ApplicationMailer < ActionMailer::Base
  default from: "asistencia.raul.campos@gmail.com"
  default to: "asistencia.t@hotmail.es"
  layout 'mailer'
end
