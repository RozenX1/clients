class MainMailer < ApplicationMailer

	def new_notification notification
		@notification = notification
		mail(subject:"Nuevo aviso")
	end

	def new_comment comment
		@comment = comment
		#We need to change destination depending on the user
		if comment.user == comment.notification.user
			mail(subject:"Nuevo comentario")
		else
		 	mail(to: comment.notification.user.email, subject:"Nuevo comentario")
		 end
	end
end
