class UserController < ApplicationController

	def update
		@user = User.find(params[:id])
		@user.update_attribute(:avatar, params[:user][:avatar])
		redirect_to :root
	end

	private

	def user_params
	  params.require(:user).permit(:avatar)
	end

end
