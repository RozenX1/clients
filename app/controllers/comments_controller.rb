class CommentsController < ApplicationController

	def create
		@comment = Comment.new(comment_params)
		@comment.notification = Notification.find(params[:notification_id])
		@comment.user = current_user
		respond_to do |format|
			if @comment.save
				MainMailer.new_comment(@comment).deliver_now
				format.json{render :show, status: :created, location: @comment.notification }
			else
				format.json { render json: @comment.errors, status: :unprocessable_entity}
			end
		end
	end

	private

	def comment_params
		params.require(:comment).permit(:notification_id, :body)
	end
end
