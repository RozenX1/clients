class NotificationsController < ApplicationController
	before_action :find_notification, only: [:show, :destroy, :toggle_solved]
	before_action :authenticate_user!, only: [:index, :show]

	#GET /notifications
	def index
		if current_user.is_admin?
			@notifications = Notification.all
		else
			@notifications = Notification.search_for_user current_user
		end
		@paged_notifications = @notifications.reversed.paginate page: params[:page], per_page:4
	end

	#GET /notifications/new
	def new
		@notification = Notification.new
	end

	#GET /notification/:id
	def show
		@notification.user = User.find(@notification.user_id) unless @notification.user.nil?
	end

	#POST /notification
	def create
		@notification = Notification.new notification_params
		if current_user
			@notification.user_id = current_user.id
		end
		flash[:notice] = "Aviso creado correctamente. Responderemos en breves a tu problema. ¡Muchas Gracias!."
		@notification.save
		MainMailer.new_notification(@notification).deliver
		redirect_to :root
	end

	#DELETE /notification/:id
	def destroy
		@notification.comments.each{|c| c.destroy}
		@notification.destroy
		redirect_to action:"index"
	end

	def toggle_solved
		@notification.toggle_solved!
		@notification.save
		render nothing: true
	end

	private

	def find_notification
		@notification = Notification.find params[:id]
	end

	def notification_params
		params.require(:notification).permit(:person, :telephone, :city, :street, :number, :appliance, :problem, :optional_photo)
	end

end
