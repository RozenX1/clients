class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :notifications
  has_many :comments
  has_attached_file :avatar
  validates_attachment_content_type :avatar, content_type:  /\Aimage\/.*\Z/

  def is_admin?
  	self.permission_level == "Admin"
  end
end
