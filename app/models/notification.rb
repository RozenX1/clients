class Notification < ActiveRecord::Base
	has_attached_file :optional_photo, styles:{thumb:"300x300>", medium:"800x800>"}
	validates_attachment_content_type :optional_photo, content_type:  /\Aimage\/.*\Z/
	before_create :mark_as_unresolved
	has_many :comments
	belongs_to :user

	scope :search_for_user, lambda{|user| where user_id:user.id}
	scope :reversed, lambda{order "created_at DESC"}

	def toggle_solved!
		self.solved = !self.solved
	end

	private

	def mark_as_unresolved
		self.solved = false
		return true
	end

end
