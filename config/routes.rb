Rails.application.routes.draw do
	
	root "welcome#homepage"
	get "/about_us" => "welcome#about_us"

  	devise_for :users
	resources :user, only:[:update]
	
	resources :notifications do
		member do
			put "toggle_solved"
		end
	
		resources :comments
	end

end
